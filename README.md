# CardCatcher

A Dreamcatcher card collection sorter.

## Goals

- Facilitate tracking of your Kpop card collection.
- Offer a overview of all possible cards. 
- Let you share your collection with other people.

## Current Implementation

This project is made specifically for the card collections for Dreamcatcher as those are the ones that interest me, but it is made in a way to allow for forking and supporting any other group with minimal code change. So feel free to do so if you wish. 

I will do me best to offer minimal support for forks aimed at other groups, but no garanties. I will however try to bugfix and improve this version. Open an issue here if you find a problem or have a suggestion.